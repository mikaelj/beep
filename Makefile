CC?=gcc
CFLAGS?=-Wall -O2
EXEC_NAME=beep
DESTDIR?=/
INSTALL_DIR=${DESTDIR}/usr/bin
MAN_FILE=beep.1.gz
MAN_DIR=${DESTDIR}/usr/share/man

default : beep

clean :
	rm ${EXEC_NAME}

beep : beep.c
	${CC} ${CFLAGS} -o ${EXEC_NAME} beep.c

install :
	install -d ${INSTALL_DIR}
	install -m 755 ${EXEC_NAME} ${INSTALL_DIR}
	# rm -f /usr/man/man1/beep.1.bz2
	install -d ${MAN_DIR}
	install -d ${MAN_DIR}/man1
	install -m 644 ${MAN_FILE} ${MAN_DIR}/man1

